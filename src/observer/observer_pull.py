"""

"""
from abc import ABCMeta, abstractmethod


class Observer(metaclass=ABCMeta):
    """
    觀察者/訂閱者
    """

    @abstractmethod
    def update(self, water_degree: float):
        pass


class Observable(metaclass=ABCMeta):
    """
    發佈者
    """

    @abstractmethod
    def add_ob(self, ob: Observer) -> None:
        pass

    @abstractmethod
    def remove_ob(self, ob: Observer) -> None:
        pass

    @abstractmethod
    def notify_obs(self) -> None:
        pass


class WeatherData:
    __degree: float = 20.0
    __humidity: float = 94.84
    __pressure: float = 66.6


class Weather(Observable):
    """天氣"""

    __obs = []
    __weather_data: WeatherData = None

    def add_ob(self, ob: Observer) -> None:
        self.__obs.append(ob)

    def remove_ob(self, ob: Observer) -> None:
        self.__obs.remove(ob)

    def notify_obs(self) -> None:
        for ob in self.__obs:
            ob.update(water_degree=self.__degree)

    def get_data(self) -> float:
        return self.__degree

    def get_humidity(self) -> float:
        return self.__humidity

    def get_pressure(self) -> float:
        return self.__pressure

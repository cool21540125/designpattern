# Observer Pattern

> The **observer pattern** define a one-to-many dependency between objects so that when one object has change state, all of its dependencies are notified automatically.

![Observer](../../images/Observer.png)


## 摘要

- 它是 物件的行為模式, `監聽模式` 又稱為 `觀察者模式`, `發布/訂閱模式`, `模型/視圖模式`, `源/監聽器模式`, `從屬者模式`
    - 但隨著時代演進, 有人認為: 觀察者模式 與 發布/訂閱模式(有產品出來取代了), 已經是不同的東西了
- 核心思維: **觀察者** 與 **被觀察者** 之間, 建立一種自動觸發的關係.
- 使用時機: 當一個物件的改變需要同時改變其他物件時(而且不知道有多少
- 監聽的物件: Observer
- 被監聽的物件: Observable / Subject
- 實現了 DIP 與 OCP


# 推模型 v.s. 拉模型

推模型 和 拉模型 是殺毀?

每當 Observable 狀態改變時, 即刻執行 notify()

那麼, 到底要 notify 什麼內容?

如果 Observable 只是個單純的某個溫度改變, 或是某些細微的變化

那大不了就把變化後的值 notify 出去 (推模型)

但如果 Observable 發生了徹頭徹尾的, 還需要把所有事情的始末一一告知嗎?

更有效率的解法是, 它 notify Observer: 「我發生變化了, 快來一看究竟吧」

然後 Observer 接收到消息之後(也就是 update() 後)

再向 Observable 作 getState(), 來拿到最新資訊 (拉模型)

最後, 要推要拉, 還是看業務邏輯來做決定.

    拉模型

    主管懷疑小明上班是否都在摸魚
    吩咐秘書隨時監控小明
    小明如果任何一舉一動就打暗號給我
    我再來詢問你 他又幹了啥蠢事 (這個動作其實就是 拉模型)

    但是, 小明根本不會有 get_state() 阿!!!!!

    所以像這種需求, 只能做 推模型

    此外, 拉模型 感覺上又有那麼 **不用那麼即時的** 感覺

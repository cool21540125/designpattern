# C# 實作


- 介面規範  \<\<interface>>
- 美麗人妻  Aiyu
- 被代理人  Bruce
- 代理人    Michael
- 主程式 (客戶端)

介面(IGive) 規範:
```cs
// 抽象行為  *送花, 送巧克力, 說我愛妳* 的介面
interface IGive
{
    void GiveFlower();
    void GiveChocoloat();
    void SayILoveYou();
}
```

美麗人妻 Aiyu
```cs
class BeautifulGirl
{
    public string name;
}
```

被代理人 Bruce
```cs
// 被代理人 Bruce (他才是真正的追求者) 只是他是 sula 不敢自己來
class Pursuit : IGive
{
    BeautifulGirl mm;

    public Pursuit(BeautifulGirl mm)
    {
        this.mm = mm;
    }

    public void GiveFlower()
    {
        Console.WriteLine("不敢送花");
    }

    public void GiveChocoloat()
    {
        Console.WriteLine("不敢送巧克力");
    }

    public void SayILoveYou()
    {
        Console.WriteLine("不敢說我愛妳");
    }
}
```

代理人 Michael
```cs
// 代理人 Michael 要幫忙實作這些介面行為
class Proxy : IGive
{
    Pursuit sula;   // 代理對象 對 真實對象 進行封裝

    public Proxy(BeautifulGirl mm)
    {
        this.sula = new Pursuit(mm);
    }

    public void GiveFlower()
    {
        // 代理對象 可以在執行 真實對象 操作時, 附加其他操作(封裝起來操作)
        Console.WriteLine("送花");
        // 代理人還可以幫忙做其他事情~~
    }

    public void GiveChocoloat()
    {
        Console.WriteLine("送巧克力");
        // 代理人還可以幫忙做其他事情~~
    }

    public void SayILoveYou()
    {
        Console.WriteLine("說我愛妳");
        // 代理人還可以幫忙做其他事情~~
    }
}
```

主程式
```cs
class Program
{
    static void Main(string[] args)
    {
        // 有一個美麗的小女孩~ 她的名字叫做 Aiyu
        BeautifulGirl mm = new BeautifulGirl();
        mm.name = "Aiyu";

        Proxy bb = new Proxy(mm);           // 代理人 Michael
        //Pursuit bb = new Pursuit(mm);     // 被代理人 Bruce

        // 帥哥 Michael 努力的 幫忙做代理
        bb.GiveFlower();
        bb.GiveChocoloat();
        bb.SayILoveYou();

        Console.Read();
    }
}
```

# 範例二

網頁載入時, 不會一開始就傻傻的載入圖片或影片, 而是會把網頁框架 && 圖片先整體載入, 之後圖片, 影片, 聲音, 等等大型檔案 先透過 `代理物件` 幫他們先把空間預留, 之後再慢慢載入即可

## C# 實作
- 介面規範      \<\<interface>>
- 真實物件      RealImage
- 代理物件      ProxyImage
- 主程式 (客戶端)

介面規範(ILoadImage)
```cs
interface ILoadImage
{
    void DisplayImage();
}
```

真實物件
```cs
class RealImage : ILoadImage
{
    private String fileName;

    public RealImage(String fileName)
    {
        this.fileName = fileName;
        LoadingImageFromDisk(fileName);
    }

    public void DisplayImage()
    {
        Console.WriteLine("把圖片秀出來");
    }

    private void LoadingImageFromDisk(String fileName)
    {
        Console.WriteLine("從磁碟慢慢載入圖片");
    }
}
```

代理物件
```cs
class ProxyImage : ILoadImage
{
    private String fileName;
    RealImage realImage;

    public ProxyImage(String fileName)
    {
        this.fileName = fileName;
    }

    public void DisplayImage()
    {
        if (realImage == null)
        {
            realImage = new RealImage(fileName);
        }
        realImage.DisplayImage();
    }
}
```

主程式
```cs
class Program
{
    static void Main(string[] args)
    {
        ILoadImage pp = new ProxyImage("abc.png");
        pp.DisplayImage();

        Console.Read();
    }
}
```

# 範例三

台灣沒賣 Mac產品, 台灣沒有直營店, 但有代購業者代買~  所以直接找代購

## C# 實作
- 介面規範
- 真實物件(我)
- 代理物件
- 主程式 (客戶端)

介面
```cs
interface Subject
{
    void BuyMac();
}
```

真實物件(我)
```cs
class RealSubject : ISubject
{
    public void BuyMac()
    {
        Console.WriteLine("去買Mac");
    }
}
```

代理物件
```cs
class ProxySubject : ISubject
{
    public void BuyMac()
    {
        RealSubject r = new RealSubject();
        r.BuyMac();
        this.WrapMac();     // 封裝真實物件後, 額外幫忙做其他事情
    }

    private void WrapMac()
    {
        Console.WriteLine("把 Mac 包裝起來");
    }
}
```

主程式
```cs
class Program
{
    static void Main(string[] args)
    {
        ProxySubject p = new ProxySubject();
        p.BuyMac();

        Console.Read();
    }
}
```

# Proxy Pattern

> 

![Proxy](../../images/Proxy.png)


## 摘要 

- 為啥不能自己來, 而是要透過代理?
    - 在許多情況下, 可能得訪問遠端機器, 或是存取大型檔案, 或是為了隱藏自己(安全性疑慮)...等等, 如果自己來, 可能會讓系統設計的結構變得意外的複雜. 
    - ex: 台灣沒有 Apple專賣店(RealSubject), 但有 Apple代理商(Proxy), 買 Apple, 找 Proxy 就好了
- 代理模式常見用途:
    - Logging
    - Caching
    - Access validation
- 代理模式分類:
    - Remote Proxies: Providing a local representation for an object that is in a different address space.
    - Virtual Proxies : Delaying the creation and initialization of expensive objects until needed, where the objects are created on demand.
        - ex: 載入網頁時, 圖片都還沒出來, 但是圖片的空間會先被一個代理物件先預留下來, 然後再異步載入, 加速存取
        - ex: 代理物件 暫時不去 實例化 真實物件, 直到 代理物件 的某個方法被調用時才做.
    - Protection Proxies
    - Smart References


## 生活上常見的範例

*Bruce* 喜歡 *Aiyu*, 但是他太害羞不敢表達, 所以請了 *Michael* 幫他 *送花, 送巧克力, 說我愛妳*, 但是 *Aiyu* 不知道原來幕後是 *Bruce* 在主導
    
- Bruce, `RealSubject` : **被代理對象** ; **被代理人** ; **真實對象** ; **最終引用的對象** ; **目標對象**
- Michael, `Proxy` : **代理對象** ; **代理人**
- Aiyu : 好像沒有個特定的名詞..., 就暫時稱為 **美麗人妻**

從上面故事得知, *代理人* 需要幫忙從事一些行為 : *送花, 送巧克力, 說我愛妳*. 

*代理人* 是誰無所謂, *代理人* 只要幫 *被代理人* *送花, 送巧克力, 說我愛妳* 給 *美麗人妻* 就可以了. 

所以需要把 *送花, 送巧克力, 說我愛妳* 這些行為抽象出來, 然後讓 *代理人* 來實作這些行為.

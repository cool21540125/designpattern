# Facade Pattern, 面板模式 / 門面模式

> Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.

白話文就是: API 的概念啦!!

## 摘要
- When to use? -> 當你要存取一個複雜的系統時, 會很需要 facade
- 在 C# 裡頭, 這個模式也被稱為 **Table Data Gateway**
- 與 Mediator Pattern 的比較:
    - **Facade Pattern**(Structural) 從上施加規約. 使用上是 可見且具強制性
        - 大家都同意 並約定 統一透過 Facade (約定關注點) 來進行操作
        - 封裝了 存取子系統 的方式
    - **Mediator Pattern**(Behavioral) 從下施加規約. 使用上是 隱藏且自由的
        - 被限制規約的物件們, 彼此之間未必知道 Mediator 的存在
        - 此種規約是既定事實而非約定
        - 封裝了各個 元件/類別 彼此之間的互動
- [值得一看的問題](https://stackoverflow.com/questions/47083977/design-patterns-understanding-facade-pattern?rq=1)

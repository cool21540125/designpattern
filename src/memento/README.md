# Memento Pattern

> 

![]()

# 摘要

- 此模式提供了 還原物件 的能力 (藉由 undo 或 rollback)
- Memento 由 3 個物件來搭配實作:
    - Originator: 具有內部狀態 (儲存資料)
    - Caretaker: 能夠幫 Originator 紀錄他的變化, 並在必要時候可以 Undo
    - Memento: 
- 

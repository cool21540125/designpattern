"""
此非良好範例 (為了舉例而故意生出來的範例)
"""
from abc import ABCMeta, abstractmethod


class IMediator(metaclass=ABCMeta):
    @abstractmethod
    def send(self, msg: str, to: str):
        pass

    @abstractmethod
    def send_to_all(self, msg: str):
        pass


class MessageMediator(IMediator):
    _members: list = []

    def join_member(self, member) -> None:
        """
        member: Messenger
        """
        self._members.append(member)

    def send(self, msg: str, source: str, to: str) -> None:
        pass

    def send_to_all(self, msg: str, source: str) -> None:
        for member in self._members:
            member


class Messenger(metaclass=ABCMeta):
    _name: str = ""
    _mediator: IMediator = MessageMediator()

    def __init__(self, name: str):
        self._name = name

    def send(self, msg: str, source: str, to: str) -> None:
        self._mediator.send(msg=msg, source=self._name, to=to)

    def send_to_all(self, msg: str, source: str):
        self._mediator.send_to_all()

    @property
    def name(self) -> str:
        return self._name


class CommonUser(Messenger):
    def send_to_all(self, msg: str, source: str) -> None:
        raise Exception("非為 VIP, 無法廣播發送")


class VIPUser(Messenger):
    pass

"""
聊天室聊天
User 說話後, 所有 User 的 ChatRoom 上都會顯示 某個User 說了什麼
(範例沒有舉得很好, 看起來好像就只是一般的依賴注入而已...)
"""
from abc import ABCMeta, abstractmethod


class Mediator(metaclass=ABCMeta):
    @abstractmethod
    def add_message(self, msg: str):
        pass

    @abstractmethod
    def display(self, user, msg: str):
        pass


class ChatRoom(Mediator):
    def add_message(self, user, msg: str) -> None:
        self._msg = f"[{user}]: {msg}"

    def display(self) -> str:
        return self._msg


class User:
    def __init__(self, name: str, mediator: Mediator):
        self._name = name
        self._mediator: Mediator = mediator

    def __str__(self) -> str:
        return self._name

    def say(self, msg: str):
        self._mediator.add_message(user=self, msg=msg)

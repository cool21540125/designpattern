"""
地球史上最虛偽的民主政權(比獨裁更為噁心) 努力壓制 西藏, 香港, 台灣 的聲浪

然後因為這個組織太廢了, 各個部門之間的溝通方式往往一團亂, 又或者某人想要搞集權
所以, Military/Police/KMT/CCTV...等, 全都透過 Xi 來做統一調度
"""
from abc import ABCMeta, abstractmethod


class Centralization(metaclass=ABCMeta):
    """中央集權"""

    @abstractmethod
    def dispatch(self, msg: str, department):
        """
        department: CountryMachine
        """
        pass


class CountryMachine(metaclass=ABCMeta):
    def __init__(self, mediator: Centralization):
        self._mediator = mediator


class Xi(Centralization):
    """紅色維尼"""

    def dispatch(self, msg: str, unit: CountryMachine):
        pass


class Military(CountryMachine):
    """阿兵哥們"""

    def attack(self):
        return "開火"


class PoliceDept(CountryMachine):
    """黑警系統"""

    def arrest(self):
        return "打自家人"


class KMT(CountryMachine):
    """很直覺不解釋"""

    pass


class CCTV(CountryMachine):
    """淪陷區央視"""

    def broadcast(self):
        pass

    def brain_washing(self):
        return "都是別人不好, 黨是萬能的"

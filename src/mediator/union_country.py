"""
UnitedNations 旗下有 WHO

USA, Japan 都加入了
"""
from abc import ABCMeta, abstractmethod

"""
class UnitedNations(metaclass=ABCMeta):
    @abstractmethod
    def set_state(self, country):
        pass
    
    @abstractmethod
    def send(self, msg: str, sender):
        # sender: Country
        pass


class Country(metaclass=ABCMeta):
    def __init__(self, mediator: UnitedNations):
        self._mediator = mediator
    
    @abstractmethod
    def send(str, msg: str):
        pass
    
    @abstractmethod
    def receive(self):
        pass


class Japan(Country):
    def send(self, msg: str) -> None:
        self._mediator.send(msg=msg, sender=self)
        
    def receive(self, msg: str) -> None:
        print(f"收到情报: {msg}")


class TW(Country):
    def send(self, msg: str) -> None:
        self._mediator.send(msg=msg, sender=self)
        
    def receive(self, msg: str) -> None:
        print(f"收到情報: {msg}")


class USA(Country):
    def send(self, msg: str) -> None:
        self._mediator.send(msg=msg, sender=self)
        
    def receive(self, msg: str) -> None:
        print(f"Receive information: {msg}")


class WHO(UnitedNations):
    _tw: Country = None
    _usa: Country = None
    _japan: Country = None
    
    @property.setter
    def usa(self, country: Country):
        self._usa = country
    
    @property
    def usa(self):
        return self._usa
    
    @property.setter
    def tw(self, country: Country):
        self._tw = country
    
    @property
    def tw(self):
        return self._tw
    
    @property.setter
    def japan(self, country: Country):
        self._japan = country
    
    @property
    def japan(self):
        return self._japan
"""

"""
想像畫面中有 4 個 FormItem
- Button : 點選後, Option (不選取) && TextBox 清空
- Option : 核取後, TextBox 帶預設字串
- TextBox : 可以輸入文字
- CheckBox : 選取後, 除了自己以外的 FormItem 全部隱藏
彼此之間只需要知道他們都是透過 Mediator 來做協調就好
"""
from abc import ABCMeta, abstractmethod


class FormItem(metaclass=ABCMeta):
    pass


class Button(FormItem):
    def __init__(self, mediator = None):
        """mediator: Mediator"""
        self._name = "輸入"
        self._power: bool = False
        self._mediator = mediator

    def click(self) -> None:
        if self._mediator:
            self._mediator.click()


class TextBox(FormItem):
    def __init__(self, content: str, mediator):
        self._content: str = content
        self._visible: bool = True
        self._mediator = mediator

    def __str__(self):
        return self._content if self._visible else ""

    def show(self):
        self._visible: bool = True

    def hide(self):
        self._visible: bool = False

    @property
    def content(self) -> str:
        return self._content

    @content.setter
    def content(self, content: str) -> str:
        self._content = content


class CheckBox(FormItem):
    """用來隱藏其他所有 FormItem"""

    def __init__(self, mediator):
        self._mediator = mediator

    def check(self) -> None:
        self._hidden_all = True

    def uncheck(self) -> None:
        self._hidden_all = False

    @property
    def hidden_all(self) -> bool:
        return self._hidden_all


class Option(FormItem):
    """TextBox是否使用預設字串"""
    def __init__(self, mediator):
        self._default = False
        self._mediator = mediator
        
    @property
    def default(self) -> bool:
        return self._default
        
    @default.setter
    def default(self, default: bool) -> None:
        self._default = default
    

class IMediator(metaclass=ABCMeta):
    pass


class EntryMediator(IMediator):

    def set_button(self, button: Button):
        self._button = button
        
    def set_check_box(self, check_box: CheckBox):
        self._check_box = check_box
        
    def set_option(self, option: Option):
        self._option = option

    def set_text_box(self, text_box: TextBox):
        self._text_box = text_box
    
    def click(self):
        if self._check_box:
            self._check_box.uncheck()
        if self._option:
            self._option.default = False
        if self._text_box:
            self._text_box.content = ""

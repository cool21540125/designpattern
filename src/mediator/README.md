# Mediator Pattern

> Define an object that encapsulates how a set of objects interact. It promotes loose coupling by keeping objects from referring to each other explicitly, and it allows their interaction to be varied independently. Client classes can use the mediator to send messages to other clients, and can receive messages from other clients via an event on the mediator class.


![Mediator](../../images/Mediator.png)


## 摘要

- 將 Colleagues 之間的相互行為, 交由 Mediator 來做調停, 因而減輕 Colleagues 之間的 溝通複雜度(decouple)
    - **避免 Objects 成員們之間, 相互引用導致錯綜復雜的不必要耦合, 進而使 物件之間, 可以獨立的變化, 來達到鬆耦合**
    - 但是隨著物件們的增長, 調停者 職責將會變得越來越複雜, 越來越難以維護是必然的
- 又稱為 Behavioral Pattern.
- Mediator Pattern 現實狀況 && 實際應用:
    - Countrys 與 United Nations
    - Airplain 與 Airport control tower
- Mediaor Pattern 主要重要角色:
    - Mediator         : 定義了讓 Colleague 可互相溝通的介面
    - ConcreteMediator  : 實作 Mediator && 幫助 Colleague 實例們互相溝通, 他知道所有的 ConcreteColleague 以及 他們之間相互溝通的目的
    - Colleague        : 定義了一個可讓其他 Colleague 透過他的 Mediator 相互溝通的介面
    - ConcreteColleague : 實作 Colleague && 透過 Colleague 裡頭的 Mediator, 與其他 ConcreteColleague 相互溝通
        - 可以把這個理解成, 兩個不同類別(服務)
- 如果此類的需求, 不採用 Mediator Pattern 來設計的話, 則需要把跟彼此溝通的方式, 耦合地寫入到各個類別之中, 將會導致類別們異常龐大且相互糾葛不清
- 與 **Facade Pattern** 比較, 詳 `/facade/README.md`
- 與 **Responsibility**, **Command**, **Observer** 模式都用來處理 Producer && Consumer 之間的關係
    - Mediator : 清除了 Producer && Consumer 之間的直接調用關係. 使 類別 的功能回歸最單純的狀態
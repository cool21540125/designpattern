"""
不遵照 Command Pattern && DIP 情況的寫法
"""


class TV:
    _monitor: str = ""

    def power_on(self) -> None:
        self._monitor = "名嘴講幹話"

    def power_off(self) -> None:
        self._monitor = ""

    def __str__(self):
        return self._monitor


class RemoteControl:
    """超沒水準的遙控器"""

    _power = False

    def turn_on(self) -> None:
        self._power = True

    def turn_off(self) -> None:
        self._power = False

    @property
    def state(self):
        return self._power

# Command Pattern

> Encapsulate a request as an object, thereby letting you parametrize clients with different requests, queue or log requests, and support undoable operations.

![CommandPattern](../../images/Command.png)


## 摘要

- 物件 封裝了 執行特定動作 or 稍後驅動特定事件 的所有必要資訊.
  - 必要資訊包含方法名稱
- 扯到 Command Pattern, 通常會提到 4 個名詞:
  - Client   : 添加命令的那個東西(給 Invoker)
  - Invoker  : 負責統籌發布命令的那個東西 (叫各個 Receiver 來做事)
  - Command  : 每個命令其實早就內定好它所屬的 Receiver, 一旦確認執行命令, 便會透過 Invoker 調用(invoke) Receiver 的行為
  - Receiver : 執行命令的那個東西 (Command 會來調用它), 就是被安排來執行命令的那些辛苦勞工們
- 上圖的 Receiver 也可是個 抽象類別, 底下還有實際負責接收的 具體類別
- 使用情境: 想要藉由統一介面發號施令, 然後根本不需要知道誰是命令的執行者
  - 基本上, 遊戲/DBMS/設備...等等 都會用到, 可說是最普遍存在的設計模式
- Coding 的流程有點像是 `Invoker.publish() -> Command.execute() -> Receiver.do_something()`

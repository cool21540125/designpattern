"""
這個範例參考自 command pattern wiki, C# 版本

有一支很鳥的遙控器, 他只能 「開啟/關閉」, 沒有其他功能了

電視也非常鳥, 打開以後只能聽飽劫在那邊講垃圾話

最直覺也會想到, 我們起碼要有 RemoteControl 及 TV
"""
from abc import ABCMeta, abstractmethod


class Command(metaclass=ABCMeta):
    """抽象命令 / Command"""

    @abstractmethod
    def execute(self):
        pass


class ISwitchable(metaclass=ABCMeta):
    """Abstract Receiver"""

    @abstractmethod
    def power_on(self):
        pass

    @abstractmethod
    def power_off(self):
        pass


class TV(ISwitchable):
    """命令的執行類別 / Receiver"""

    _monitor: str = ""

    def power_on(self) -> None:
        self._monitor = "名嘴講幹話"

    def power_off(self) -> None:
        self._monitor = ""

    def __str__(self):
        return self._monitor


class CloseCmd(Command):
    """關閉按鈕"""

    def __init__(self, target: ISwitchable):
        self._target = target

    def execute(self) -> None:
        self._target.power_off()


class OpenCmd(Command):
    """開啟按鈕"""

    def __init__(self, target: ISwitchable):
        self._target = target

    def execute(self) -> None:
        self._target.power_on()


class RemoteControl:
    """操作的開關(遙控器啦) / Invoker"""

    def __init__(self, open_cmd: OpenCmd, close_cmd: CloseCmd):
        self._open_cmd = open_cmd
        self._close_cmd = close_cmd

    def turn_on(self) -> None:
        self._open_cmd.execute()

    def turn_off(self) -> None:
        self._close_cmd.execute()

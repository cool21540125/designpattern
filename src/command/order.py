"""
參考 大話設計模式 的範例
餐廳的 訂餐人員(Waitress/Invoker) 幫忙 點餐(寫訂單 IOrder)
然轟送 訂單(IOrder) 到櫃檯請 酒保(BarKeeper) 送酒(假設只有 Wiskey), 
然後送 訂單(IOrder) 到廚房請 廚師(Chef)      處理食材(假設只有 牛排)
"""
from abc import ABCMeta, abstractmethod
from typing import Any


class RestaurantWorker(metaclass=ABCMeta):
    pass


class IOrder(metaclass=ABCMeta):
    """Command Interface"""

    def __init__(self, handler: RestaurantWorker):
        self._handler = handler

    @abstractmethod
    def execute(self):
        pass


class Chef(RestaurantWorker):
    def cook_steak(self) -> str:
        return "牛排"

    def stir_pork(self) -> str:
        return "炒豬肉"


class BarKeeper(RestaurantWorker):
    def prepare_wiskey(self) -> str:
        return "Wiskey"


class Waitress(RestaurantWorker):
    _orders: list = []

    @property
    def show_orders(self) -> list:
        return self._orders

    def add_order(self, order: IOrder):
        self._orders.append(order)

    def remove_order(self, order: IOrder):
        self._orders.remove(order)

    def send_orders(self) -> None:
        for oo in self._orders:
            oo.execute()
        self._orders = []


class Wiskey(IOrder):
    def execute(self) -> str:
        return self._handler.prepare_wiskey()


class Beef(IOrder):
    def execute(self) -> str:
        return self._handler.cook_steak()


class Pork(IOrder):
    def execute(self) -> str:
        return self._handler.stir_pork()

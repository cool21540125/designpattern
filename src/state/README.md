# State Pattern

> 

![StatePattern](../../images/State.png)

## 摘要
- UML 圖, 和 Strategy Pattern 有 99.87% 一樣.



## State Pattern v.s. Strategy Pattern

- 策略模式 著重於 不同的 XX (策略) 可以相互取代, 來完成一件事情 (打人的方式)
- 狀態模式 著重於 不同的 XX (狀態) 的情境, 做事情的方式會不一樣 (精神飽滿時把人揍飛, 很累時就不打人)

- 策略模式 封裝了 不同 策略的選擇 的 演算法
- 狀態模式 封裝了 不同 狀態的變化 的 行為


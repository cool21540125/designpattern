"""
隨著時間經過, 工作效率會不斷變化
"""
from abc import ABCMeta, abstractmethod


class WorkingState(metaclass=ABCMeta):
    """工作狀態"""

    @abstractmethod
    def work(self):
        pass


class PowerfulWorkingState(WorkingState):
    def work(self):
        return 100


class GoodWorkingState(WorkingState):
    def work(self):
        return 80


class TiredWorkingState(WorkingState):
    def work(self):
        return 40


class NoPowerWorkingState(WorkingState):
    def work(self):
        return 5


class Worker:
    def __init__(self):
        self._state = GoodWorkingState()

    def do_work(self):
        return self._state.work()

    def change_state(self, hour: int):
        if hour < 12:
            self._state = PowerfulWorkingState()
        elif hour < 16:
            self._state = GoodWorkingState()
        elif hour < 20:
            self._state = TiredWorkingState()
        else:
            self._state = NoPowerWorkingState()

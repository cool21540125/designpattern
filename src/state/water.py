"""
Water(也就是 H2O) 的三種狀態

水蒸氣, 水, 冰
"""
from abc import ABCMeta, abstractmethod


class Environment:
    """當下的環境, Context 啦!!"""


class State(metaclass=ABCMeta):
    pass


class SolidState(State):
    def __str__(self) -> str:
        return "冰塊"


class LiquidState(State):
    def __str__(self) -> str:
        return "湯湯水水"


class GaseousState(State):
    def __str__(self) -> str:
        return "水蒸氣"


class Water:
    """會改變狀態的水"""

    _state: State

    def set_temperature(self, fahrenheit: int) -> None:
        if fahrenheit >= 100:
            self._set_state(state=GaseousState())
        elif fahrenheit <= 0:
            self._set_state(state=SolidState())
        else:
            self._set_state(state=LiquidState())

    def _set_state(self, state: State) -> None:
        self._state = state

    def __str__(self):
        return str(self._state)

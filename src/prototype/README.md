# Prototype Pattern

![](../../images/Prototype.png)

- 如果物件初始化的過程開銷很大, 採用 Prototype 可避免此動作, 取而代之的是在 run time 期間直接取得物件的最新狀態直接製作一個副本

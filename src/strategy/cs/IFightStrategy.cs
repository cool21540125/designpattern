namespace StrategyMode
{
    public interface IFightStrategy
    {
        void Execute();
    }
}
"""
工廠方法模式來生產 pizza

pizza 有 台式 & 美式 pizza

美式 分為 重乳酪口味 糖分加倍口味
台式 分為 台式泡菜口味 鳳梨酥口味
"""
from abc import ABCMeta, abstractmethod


class IPizza(metaclass=ABCMeta):
    pass


class IUSPizza(IPizza, metaclass=ABCMeta):
    pass


class ITWPizza(IPizza, metaclass=ABCMeta):
    pass


class USHeavyCheezePizza(IUSPizza):
    pass


class USDoubleSugarPizza(IUSPizza):
    pass


class TWPineApplePizza(ITWPizza):
    pass


class TWPicklePizza(ITWPizza):
    pass


class IPizzaStore(metaclass=ABCMeta):
    @abstractmethod
    def create_pizza(self):
        pass


class USPizzaStore(IPizzaStore):
    def create_pizza(self, style: str):
        if style == "cheeze":
            return USHeavyCheezePizza()
        elif style == "sugar":
            return USDoubleSugarPizza()


class TWPizzaStore(IPizzaStore):
    def create_pizza(self, style: str):
        if style == "pickle":
            return TWPicklePizza()
        elif style == "pineapple":
            return TWPineApplePizza()

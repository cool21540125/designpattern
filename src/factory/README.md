# Factory

有 3 種工廠

## Simple Factory Pattern

![SimpleFactory](../../images/SimpleFactory.png)

> Define an interface for creating an object, it through the argument to decide which class to instantiate.

- 違反 OCP
- 其實, 簡單工廠 比較像是個習慣, 而非設計模式.
- 要生產啥產品的邏輯, 定義在 Factory 內部


## Factory Method Pattern

![FactoryMethod](../../images/FactoryMethod.png)

> Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.

- 處理掉 Simple Factory 違反 OCP 的問題
- 實踐了 DIP
- 將 Simple Factory 進行專業化生產
- 對 implementation & use 解耦
- 比起 Simple Factory, 此模式在 IFactory 之中多了個 抽象方法, 讓子類別來實踐 implementation(子類別 來決定要實體化哪種類別)
- 把 Simple Factory 內部邏輯判斷(要生產啥產品), 交由 Client 來決定
- 缺點就是, 如果新增加一個 Product, 也必須伴隨著增加一個 Factory
- 通常在設計階段, 會先使用此方式, 隨著商業邏輯開始複雜, 再開始考慮使用 *Abstract Factory*, *Prototype*, *Builder*, 等其他建造模式


## Abstract Factory Pattern

> Provide an interface for creating families of related or dependent objects without specifying their concrete classes.

![AbstractFactory](../../images/AbstractFactory.png)

Def: 提供一個用來建立 一系列 相關/獨立 物件的 介面, 而不用指定這些物件的具體類別

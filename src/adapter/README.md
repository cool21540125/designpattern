# Adapter Pattern

> An adapter allows two incompatible interfaces to work together. This is the real-world definition for an adapter. Interfaces may be incompatible, but the inner functionality should suit the need. The adapter design pattern allows otherwise incompatible classes to work together by converting the interface of one class into an interface expected by the clients.

![Adapter](../../images/Adapter.png)


# 摘要

- 又稱為 Wrapper Pattern.
- 將 一個介面(Adaptee) 轉換成 另一個介面(ITarget), 以符合 客戶端(Adapter) 需求
- 如上圖, *ITarget* 可以是: 實體類別 or 抽象類別 or 介面
- 如圖, 轉接器模式 分為兩種:
    - Class Adapter Pattern: 透過 **多重繼承** 對不同介面相互匹配 (C++, Python)
        - 實作方式是在 Compile Time 藉由 繼承 Adaptee 達到效果
    - Object Adapter Pattern: 透過 **繼承** && **介面實作** 來匹配 不同物件 (C#, Java, ...)
        - 實作方式是在 Run Time 藉由 delegate Adaptee 到 Adapter
- 如果內部開發(一切都是自己人可以控制的), 類別相互結合發現無法匹配時, **不應該**優先考慮 Adapter Pattern, 而是應該考慮 **重構統一介面**. 除非開發案有第三方參與(不是我們自己人說了算), 才考慮使用 Adapter (此方式只是 權宜之計/必要之惡)
    - 醫學的觀點, 會用到轉接器通常可能是病入膏肓了, 因為沒有及早治療(規範統一介面)
    - 現實的觀點, 就是因為沒有強力規範 連接線的規格, 市面上才會有那麼多討人厭的 轉接頭
- C# 比較著名的實現便是 DataAdapter
    - 用作 DataSet 內的資料 與 資料來源(DB, file, ...) 相匹配
- 實作的 Adapter 除了要實作 ITarget 之外, 還得支援 polymophic, 才是個合格的 Adapter. 此外還有底下的替代方案:
    - 另一種替代方案, 可以使用 Decorator, 在 run time 動態的為 Adaptee 添增職責, 以符合 ITarget 的要求
    - 另一種更為簡單暴力的做法, 則是使用 Facade.
    - Proxy
- Adapter 很容易跟底下的模式搞混 (都有把 Object 塞到另一個 Object constructor 的動作):
    - Decorator: 動態的為物件添加功能.
    - Facade: 透過一個介面, 來驅動一系列的複雜操作, 不要讓外界看到複雜的過程
    - Proxy: 讓一個替代物件幫忙做事情, 對外界隱藏幕後黑手

## Adapter v.s. Decorator
- Decorator: 把物件封裝起來, 賦予它們新的職責
- Adapter: 把物件封裝起來, 讓他們看起來再也不像自己(對外介面改變了)

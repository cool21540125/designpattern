/*
這個是 Clean Code 的範例
*/
using System;
using System.Windows.Forms;


public class QuickEntryMediator 
{
    private TextBox itsTextBox;
    private ListBox itsList;

    public QuickEntryMediator(TextBox t, ListBox i) 
    {
        itsTextBox = t;
        itsList = i;
        
        // 默默的把 itsTextBox 綁定 itsList
        // itsList 便可動態的呈現出 itsTextBox 所希望顯示的東西
        itsTextBox.TextChanged += new EventHandler(TextFieldChanged);
    }

    private void TextFieldChanged(object source, EventArgs args)
    {
        string prefix = itsTextBox.Text;

        if (prefix.Length == 0) 
        {
            itsList.ClearSelected();
            return;
        }

        ListBox.ObjectCollection listItems = itsList.Items;
        bool found = false;

        for (int i = 0; found == false && i < listItems.Count; i++)
        {
            Object obj = listItem(i);
            String s = obj.ToString();
            if (s.StartsWith(prefix))
            {
                itsList.SetSelected(i, true);
                found = true;
            }

            if (!found)
                itsList.ClearSelected();
        }
    }
}
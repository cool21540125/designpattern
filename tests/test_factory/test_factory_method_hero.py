import pytest
from factory.factory_method_hero import (
    TrainingCamp,
    MageTrainingCamp,
    WarrierTrainingCamp,
    Warrier,
    Mage,
)


class TestFactoryMethod:
    def test_mage_camp_trains_mage(self):
        mage_camp: TrainingCamp = MageTrainingCamp()
        swei = mage_camp.train(name="swei")
        assert swei.attack() == -9999
        ivan = mage_camp.train(name="ivan")
        assert ivan.attack() == -9999
        assert isinstance(swei, Mage)
        assert isinstance(ivan, Mage)

    def test_warrier_camp_trains_warrier(self):
        warrier_camp: TrainingCamp = WarrierTrainingCamp()
        yen = warrier_camp.train(name="yen")
        assert yen.attack() == -1
        assert isinstance(yen, Warrier)

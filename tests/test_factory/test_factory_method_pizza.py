from factory.factory_method_pizza import (
    IPizzaStore,
    USPizzaStore,
    TWPizzaStore,
    IPizza,
    ITWPizza,
    IUSPizza,
    TWPicklePizza,
    TWPineApplePizza,
    USDoubleSugarPizza,
    USHeavyCheezePizza,
)


class TestFactoryMethodPizza:
    def test_make_tw_pizza(self):
        store: IPizzaStore = TWPizzaStore()
        tw_pizza = store.create_pizza(style="pineapple")
        assert isinstance(tw_pizza, ITWPizza)
        assert type(tw_pizza) == TWPineApplePizza
        assert not isinstance(tw_pizza, IUSPizza)

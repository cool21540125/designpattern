import pytest
from mediator.window_form import Button, TextBox, CheckBox, Option, EntryMediator


class TestWindowForm:
    @pytest.fixture
    def button(self, entry_mediator) -> Button:
        return Button(mediator=entry_mediator)

    @pytest.fixture
    def text_box(self, entry_mediator) -> TextBox:
        return TextBox(content="Hello Tony", mediator=entry_mediator)

    @pytest.fixture
    def check_box(self, entry_mediator) -> CheckBox:
        return CheckBox(mediator=entry_mediator)

    @pytest.fixture
    def option(self, entry_mediator) -> Option:
        return Option(mediator=entry_mediator)

    @pytest.fixture(scope="module")
    def entry_mediator(self) -> EntryMediator:
        en = EntryMediator()
        return en

    def test_text_box_show(self, text_box: TextBox):
        text_box.show()
        assert str(text_box) == "Hello Tony"

    def test_text_box_hide(self, text_box: TextBox):
        text_box.hide()
        assert str(text_box) == ""
        # TODO: TextBox 真的消失了

    def test_check_box_hide_all(self, check_box: CheckBox):
        check_box.check()
        assert check_box.hidden_all is True
        # TODO: 所有 FormItem 都已經消失

    def test_check_box_show_all(self, check_box: CheckBox):
        check_box.uncheck()
        assert check_box.hidden_all is False
        # TODO: 所有 FormItem 都正常出現

    def test_button_click(
        self,
        button: Button,
        text_box: TextBox,
        option: Option,
        check_box: CheckBox,
        entry_mediator: EntryMediator,
    ):
        entry_mediator.set_button(button=button)
        entry_mediator.set_check_box(check_box=check_box)
        entry_mediator.set_option(option=Option)
        entry_mediator.set_text_box(text_box=text_box)
        button.click()
        assert option.default is False
        assert str(text_box) == ""

import pytest
from mediator.communisty_party import Military, PoliceDept, CCTV, KMT, CountryMachine


class TestCommunistyParty:
    @pytest.fixture
    def military(self) -> Military:
        return Military()

    @pytest.fixture
    def police_dept(self) -> PoliceDept:
        return PoliceDept()

    @pytest.fixture
    def cctv(self) -> CCTV:
        return CCTV()

    @pytest.fixture
    def kmt(self) -> KMT:
        return KMT()

    @pytest.fixture
    def country_machine(self) -> CountryMachine:
        return CountryMachine()

    def test_1(self):
        pass

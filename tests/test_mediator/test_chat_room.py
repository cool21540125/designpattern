import pytest
from mediator.chat_foom import ChatRoom, User


class TestChatRoom:
    @pytest.fixture(scope="module")
    def chat_room(self) -> ChatRoom:
        return ChatRoom()

    @pytest.fixture(scope="module")
    def tony(self, chat_room: ChatRoom) -> User:
        return User(name="Tony", mediator=chat_room)

    @pytest.fixture(scope="module")
    def andy(self, chat_room: ChatRoom) -> User:
        return User(name="Andy", mediator=chat_room)

    def test_tony_says(self, tony: User, chat_room: ChatRoom):
        tony.say(msg="Hey Yo!")
        assert chat_room.display() == "[Tony]: Hey Yo!"

    def test_many_people_say(self, andy: User, tony: User, chat_room: ChatRoom):
        andy.say(msg="不要啊~~")
        assert chat_room.display() == "[Andy]: 不要啊~~"
        tony.say(msg="哭夭阿")
        assert chat_room.display() == "[Tony]: 哭夭阿"

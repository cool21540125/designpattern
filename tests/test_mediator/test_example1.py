import pytest
from mediator.example1 import CommonUser, VIPUser, MessageMediator


class TestMediatorExample1:
    @pytest.fixture(scope="module")
    def user_1(self) -> CommonUser:
        return CommonUser(name="Tony")

    @pytest.fixture(scope="module")
    def user_2(self) -> CommonUser:
        return CommonUser(name="Kevin")

    @pytest.fixture(scope="module")
    def user_vip(self) -> VIPUser:
        return VIPUser(name="Scott")

    @pytest.fixture
    def mediator(self) -> MessageMediator:
        return MessageMediator()

    def test_normal_user_send_message(self, user_1: CommonUser, user_2: CommonUser):
        user_1.send(msg="嗨~ 你這小智障", source="Tony", to="Kevin")

    def test_vip_user_send_message(self, user_vip: VIPUser, user_1: CommonUser):
        user_vip.send(msg="嗨~ 瞧你這窮酸樣", source="Tony", to="Kevin")

    # def test_vip_user_broadcast(self, user_vip: VIPUser, user_1: CommonUser, user_2: CommonUser):
    #     mediator.join_member(member=user_1)
    #     mediator.join_member(member=user_2)
    #     user_vip.send_to_all(msg="我說在座的各位, 都是白癡", source="Scott")

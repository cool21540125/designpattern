import pytest
from command.order import Chef, Waitress, BarKeeper, Wiskey, Beef, Pork


class TestCommandOrder:
    @pytest.fixture(scope="module")
    def girl(self) -> Waitress:
        # 服務生
        return Waitress()

    @pytest.fixture(scope="module")
    def chef(self) -> Chef:
        # 廚師
        return Chef()

    @pytest.fixture(scope="module")
    def barkeeper(self) -> BarKeeper:
        # 酒保
        return BarKeeper()

    def test_orders_clear(self, girl: Waitress, chef: Chef, barkeeper: BarKeeper):
        """
        客人跟 Waitress 點 威士忌 && 牛肉
        然後 Waitress 通知廚房準備餐點
        """
        beef = Beef(handler=chef)
        pork = Pork(handler=chef)
        wiskey = Wiskey(handler=barkeeper)

        # 服務生妹妹幫忙點餐
        girl.add_order(order=beef)
        girl.add_order(order=pork)
        girl.add_order(order=wiskey)
        girl.remove_order(order=pork)
        assert len(girl.show_orders) == 2

        girl.send_orders()
        assert girl.show_orders == []

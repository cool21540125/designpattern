import pytest
from command.tv_without_command import RemoteControl, TV


class TestRemoteCommandWithoutCommand:
    """第一版的遙控器"""

    @pytest.fixture
    def tv(self) -> TV:
        return TV()

    @pytest.fixture
    def remote_control(self) -> RemoteControl:
        return RemoteControl()

    def test_power_on_tv(self, tv: TV):
        tv.power_on()
        assert str(tv) == "名嘴講幹話"

    def test_power_off_tv(self, tv: TV):
        tv.power_off()
        assert str(tv) == ""

    def test_turn_on_tv(self, tv: TV, remote_control: RemoteControl):
        # 這個叫做, 你把遙控器點開了, 然後再去把電視打開
        remote_control.turn_on()
        if remote_control.state == True:
            tv.power_on()
        else:
            tv.power_off()
        assert str(tv) == "名嘴講幹話"

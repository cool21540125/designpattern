import pytest
from command.tv import TV, ISwitchable, Command, RemoteControl, OpenCmd, CloseCmd


class TestRemoteControl:
    @pytest.fixture
    def tv(self) -> ISwitchable:
        return TV()

    @pytest.fixture
    def open_cmd(self, tv: ISwitchable) -> Command:
        return OpenCmd(target=tv)

    @pytest.fixture
    def close_cmd(self, tv: ISwitchable) -> Command:
        return CloseCmd(target=tv)

    @pytest.fixture
    def remote_control(self, open_cmd: Command, close_cmd: Command) -> RemoteControl:
        return RemoteControl(open_cmd=open_cmd, close_cmd=close_cmd)

    def test_power_on_tv(self, tv: ISwitchable):
        tv.power_on()
        assert str(tv) == "名嘴講幹話"

    def test_power_off_tv(self, tv: ISwitchable):
        tv.power_off()
        assert str(tv) == ""

    def test_remote_control_works(self, tv: ISwitchable, remote_control: RemoteControl):
        remote_control.turn_on()
        assert str(tv) == "名嘴講幹話"

        remote_control.turn_off()
        assert str(tv) == ""

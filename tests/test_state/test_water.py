import pytest
from state.water import State, Water, SolidState, LiquidState, GaseousState


class TestWaterState:
    @pytest.fixture(scope="module")
    def water(self) -> Water:
        return Water()

    def test_ice_state(self, water: Water):
        water.set_temperature(fahrenheit=0)
        assert str(water) == "冰塊"

    def test_stream_state(self, water: Water):
        assert str(water) == "冰塊"
        water.set_temperature(fahrenheit=100)
        assert str(water) == "水蒸氣"

    def test_water_state(self, water: Water):
        assert str(water) == "水蒸氣"
        water.set_temperature(fahrenheit=60)
        assert str(water) == "湯湯水水"

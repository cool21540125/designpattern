import pytest
from datetime import datetime
from state.working import Worker, WorkingState


class TestWorkingState:
    @pytest.fixture
    def worker(self) -> Worker:
        return Worker()

    @pytest.mark.parametrize(
        "working_hour, expected_result",
        [
            (10, 100),
            (13, 80),
            (19, 40),
            (22, 5),
        ],
    )
    def test_no_power_working(
        self, worker: Worker, working_hour: int, expected_result: int
    ):
        worker.change_state(hour=working_hour)
        assert worker.do_work() == expected_result

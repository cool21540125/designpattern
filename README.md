# 設計模式相關

- [23 種設計模式的趣味解釋](http://jimmy0222.pixnet.net/blog/post/37216962-%5B%E8%BD%89%E8%B2%BC%5D-23%E7%A8%AE%E8%A8%AD%E8%A8%88%E6%A8%A1%E5%BC%8F%E7%9A%84%E8%B6%A3%E5%91%B3%E8%A7%A3%E9%87%8B)
- [Proxy Pattern-最易懂的設計模式解析](https://blog.csdn.net/carson_ho/article/details/54910472)

# 設計模式

依照 GoF, 區分為 3 種大分類:

- Creational Pattern
  - Factory Method
  - Abstract Factory
  - Singleton
  - Prototype
  - Builder
- Structural Pattern
  - Facade
  - Proxy
  - Adapter
  - Bridge
  - Composite
  - Decorator
  - Flyweight
- Behavioral Pattern
  - Observer
  - Strategy
  - Command
  - Template Method
  - State
  - Visitor
  - Memento
  - Mediator
  - Iterator
  - Chain Of Responsibility
  - Interpreter

# 常見問題

## 1. _Abstract Class_ v.s. _Interface_

準則 1:

如果希望 某個東西 可以分享 功能/狀態, 就使用 Abstract Class

如果希望 某些不相干的東西 可以有 共同的行為, 就是用 Interface

準則 2:

如果想強調 某個東西 是什麼東西(is-a), 就使用 Abstract Class

如果想強調 某個東西 會做什麼事(has-a), 就使用 Interface

## 2. What is delegate?

- 使用了 composition 代替 heritance
- Delegate 為一種基本技巧, ex: 狀態模式 & 策略模式 & 訪問者模式 都是在更加特殊的場合使用了 Delegate. Delegate 讓我們可以來模擬 mixin. (這句我不懂)
- A B 兩物件參與了請求處理過程, 接收請求的 A 將 請求 交給 B 來處理, 這樣的過程就稱為 delegate
  - 如下範例的 A 為 Printer; B 為 RealPrinter

```java
class RealPrinter {
    void print() { System.out.println("xxx"); }
}

class Printer {
    RealPrinter p = new RealPrinter();
    void print() { p.print(); }
}

public class Main {
    public static void main(String[] args) {
        Printer p = new Printer();
        p.print();
    }
}
```

## 3. What is mixin in python?

- mixin 是一種特殊的 多重繼承 (mixin 不存在於 Java, C#, ...)
- 使用的情境:
  - 如果想提供給 class 一大堆 optional features
  - 想使用 a lot of class 之間的 one particular feature
  - 想為一個類別提供很多 non-optional features, 但是希望這些 features 能分散在不同的 classes 或 modules. 因而每個 module 都是一個 feature/behavior. (目的並非重複使用, 而是區隔)

# 環境

```bash
### 增加到環境變數
$# export PYTHONPATH=src

### Coverage
$# py.test --cov=./src --cov-report=html tests/
```

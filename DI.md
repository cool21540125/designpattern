# Dependency Injection, DI
- [wikipedia-Dependency injection](https://en.wikipedia.org/wiki/Dependency_injection)
- [控制反轉 (IoC) 與 依賴注入 (DI)](https://notfalse.net/3/ioc-di)

![DI](../../images/DI.png)


## 摘要

- 軟體工程裏頭, **DI** 就是指: objectA 收到他所依賴的 objectB
  - objectA 所依賴的東西可以有很多個
- 我們稱 接收到依賴的那一方(objectA) 為 `client`, 被交付(被注入)的那一方(objectB) 為 `service`
- 實作將 service 交付給 client 的程式碼, 我們稱之為 `injector`
- 將 objectB 交付給 objectA 的過程, 稱之為 `injection`
- DI 的精神之一, 並非是讓 client 聲明他所需要什麼, 取而代之的是, 讓 injector 告訴 client 你要用哪個 service
- DI 的精神之一, 讓物件的建構及使用過程達成 *separation of concerns, SoC*
  - Soc, ex: 將 js 嵌入到 html 裏頭, html 只管好網頁看起來的骨幹, js 只管好他要怎麼動起來, 讓兩邊程式上的設計不互相干擾, 增加可讀性 && 程式碼覆用性
- DI 其實是一種 *inversion of control, IoC* 的實作
  - Ioc 是一種 設計原則 : 分離元件(components) 的 設定 與 使用, 來降低耦合
  - 軟體開發教父(Martin) 因 Ioc 這名詞很會讓人混淆, 從而與幾位大咖, 給予了一個更為具體的實作名稱: Dependency Injection.
  - IoC 與 DI, 兩者不相同!!
- client 不須知道 injector 的存在, 也不需要理會 service 如何建構, 它只知道 service interface. 將 use 與 construct 的職責分離開來


## DIP, Dependency Inversion Principle
- 高階模組不應依賴於低階模組, 兩者都應該依賴於抽象
- 抽象不應該依賴於具體實作
- 具體實作應該依賴於抽象
- 倒轉的是 **依賴關係**


## IoC, Inversion of Control
- 倒轉的是 實例 依賴 物件 的 **控制流程**


## Example

士兵 在戰場上, 需要使用 武器 來消滅敵人

武器 是個抽象的東西, 可以是 刀, 槍, 石頭, 臭襪子, ...

士兵 到 戰場 上, 除非槍砲彈藥都沒了要自己生產武器以外

正常來說都會有 槍, 所以 士兵 從來沒參與 生產武器 的過程

他只要知道如何使用 武器: 槍

士兵 用的 槍(依賴實例), 不用 士兵 自己去 生產, 而是 被動接收(政府 這個 容器 提供給 士兵)

因此, 槍(要依賴的東西) 的控制流程, 由 主動 轉變成 被動

這就是 IoC

回顧 DIP

- 士兵(高階模組) 不應依賴於 槍(低階模組), 兩者都應該依賴於 武器(抽象)
  - 槍 extends 武器; 士兵 use 武器
  - 士兵 不應該直接使用 槍

倒轉了 依賴關係 

